package test4.using.task;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class ApplicationTask {
	
	public static void main(String[] args) {
        SpringApplication.run(ApplicationTask.class);
    }

}
