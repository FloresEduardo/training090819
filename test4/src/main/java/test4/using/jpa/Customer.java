package test4.using.jpa;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
public class Customer {
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	//@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SECUENCIACUSTOMERS")
	//@SequenceGenerator(name="SECUENCIACUSTOMERS", sequenceName="SECUENCIACUSTOMERS", allocationSize=1)
    private Long codigo;
    private String firstName;
    private String lastName;
    
    protected Customer() {}

    public Customer(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }
    
    @Override
    public String toString() {
        return String.format(
                "Cliente[codigo=%d, firstName='%s', lastName='%s']",
                codigo, firstName, lastName);
    }

}
