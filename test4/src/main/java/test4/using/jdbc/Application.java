package test4.using.jdbc;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;

@SpringBootApplication
public class Application implements CommandLineRunner {

	private static final Logger log = LoggerFactory.getLogger(Application.class);

	public static void main(String args[]) {
		SpringApplication.run(Application.class, args);
	}

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Override
	public void run(String... args) throws Exception {

		log.info("Creando Tablas");
		
		//jdbcTemplate.execute("DROP TABLE customers IF EXISTS");
		/*jdbcTemplate.execute("CREATE TABLE customers(" +
                "codigo NUMBER, first_name VARCHAR(255), last_name VARCHAR(255))");*/
		//jdbcTemplate.execute("CREATE SEQUENCE secuenciacustomers START WITH 1 INCREMENT BY 1");
        /*jdbcTemplate.batchUpdate("CREATE TRIGGER trigercustomers BEFORE INSERT ON customers"
                + "FOR EACH ROW"
                + "BEGIN SELECT secuenciacustomers.NEXTVAL INTO :NEW.codigo FROM DUAL");*/

        // Split up the array of whole names into an array of first/last names
        List<Object[]> splitUpNames = Arrays.asList("Eduardo Flores", "Julio Dean", "Cesar Bloch", "Josh Walcott").stream()
                .map(name -> name.split(" "))
                .collect(Collectors.toList());

        // Use a Java 8 stream to print out each tuple of the list
        splitUpNames.forEach(name -> log.info(String.format("Agregando registros del cliente: %s %s", name[0], name[1])));

        // Uses JdbcTemplate's batchUpdate operation to bulk load data
        jdbcTemplate.batchUpdate("INSERT INTO customers(first_name, last_name) VALUES (?,?)", splitUpNames);

        log.info("Consulta de Clientes con el nombre 'Josh':");
        jdbcTemplate.query(
                "SELECT codigo, first_name, last_name FROM customers WHERE first_name = ?", new Object[] { "Josh" },
                (rs, rowNum) -> new Customer(rs.getLong("codigo"), rs.getString("first_name"), rs.getString("last_name"))
        ).forEach(customer -> log.info(customer.toString()));

	}

}
